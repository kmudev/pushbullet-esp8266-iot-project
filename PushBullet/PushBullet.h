#ifndef PUSHBULLET

#include <WiFiClientSecure.h>
#include <time.h>
#include <cstring>

struct PushBullet
{
    PushBullet(const String _apiToken, uint16_t _port, unsigned char * _rootCA, unsigned int _rootCALen);
    
    bool isConnected();
    void setTime();
    tm getTime();
    void sendNotification(const String title, const String message);

    private:
      WiFiClientSecure * secureClient;
      uint16_t port;
      unsigned char * rootCA;
      unsigned int rootCALen;
      String apiToken;
      String apiUrl = "api.pushbullet.com";

      bool setRootCA(unsigned char * certificate, unsigned int certificateLen);
      bool connectToAPI();
      bool closeConnection();
      String buildRequest(String url, String type, String title, String message);
      String getResponse();
      String getHeaders();
};

#endif
