#include <PushBullet.h>

PushBullet::PushBullet(const String _apiToken, uint16_t _port, unsigned char * _rootCA, unsigned int _rootCALen)
{
    this->apiToken = _apiToken;
    this->port = _port;
    this->rootCALen = _rootCALen;
    this->rootCA = new unsigned char[this->rootCALen];
    memcpy(this->rootCA, _rootCA, rootCALen);
    this->secureClient = new WiFiClientSecure();
    this->secureClient->setTimeout(10);
    this->setRootCA(this->rootCA, this->rootCALen);
    this->setTime();
}

bool PushBullet::setRootCA(unsigned char * certificate, unsigned int certificateLen)
{
    if(this->secureClient->setCACert_P(certificate, certificateLen))
        return true;

    Serial.println("Failed to load certificate.");
    return false;
}

bool PushBullet::connectToAPI()
{
    if(this->isConnected())
    {
        Serial.println((String)"Reusing connection to: " + this->apiUrl);
        return true;
    }
    else if(this->secureClient->connect(this->apiUrl.c_str(), port))
    {
        Serial.println((String)"Connected to: " + this->apiUrl);
        return true;
    }

    Serial.println((String)"Failed to connect to: " + this->apiUrl);
    return false;
}

bool PushBullet::isConnected()
{
    if(this->secureClient->connected())
        return true;

    return false;
}

String PushBullet::buildRequest(String endpoint, String type, String title, String message)
{
    String requestBody = "{\"type\": \"" + type + "\", \"title\": \"" + title + "\", \"body\": \"" + message + "\"}\r\n";

    String request = String("POST ") + endpoint + " HTTP/1.1\r\n" +
                            "Host: " + this->apiUrl + "\r\n" +
                            "User-Agent: ESP8266\r\n" +
                            "Authorization: Bearer " + this->apiToken + "\r\n" +
                            "Content-Type: application/json\r\n" +
                            "Content-Length: " + String(requestBody.length()) + "\r\n\r\n" +
                            requestBody;

    return request;
}
    
void PushBullet::setTime()
{
    Serial.println("Setting time using SNTP");
    // configTime takes the timezone (in seconds), the daylight saving and NTP servers
    configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
    time_t now = time(nullptr);

    while (now < 8 * 3600 * 2) 
    {
        delay(500);
        now = time(nullptr);
    }

    // structure containing a calendar date and time broken down into its components.
    tm timeinfo;

    // gmtime takes an argument of data type time_t which represents calendar time
    // It converts the calendar time 'timep' to broken-down time representation, expressed in UTC
    // _r is used to save it in our own user struct 
    gmtime_r(&now, &timeinfo);
    Serial.println((String)"Current time: " + asctime(&timeinfo));
    Serial.println((String)"In seconds: " + time(&now));
}

void PushBullet::sendNotification(const String title, const String message)
{
    if(!this->connectToAPI())
        return;

    String request = buildRequest("/v2/pushes", "note", title, message);
    Serial.println((String)"Requesting /v2/pushes");
    this->secureClient->print(request);
    Serial.println((String)"Response:\n" + getResponse());
}

String PushBullet::getHeaders()
{
    String line;
    String headers;

    while(this->isConnected())
    {
        line = this->secureClient->readStringUntil('\r');
        headers += line;

        if(line == "\n")  
            break;
    }

    return headers;
}

String PushBullet::getResponse()
{
    String response;
    String headers;

    headers = this->getHeaders();
    response += this->secureClient->readStringUntil('\r');
 
    return headers + response;
}



tm PushBullet::getTime()
{
    time_t now = time(nullptr);
    tm timeinfo;
    gmtime_r(&now, &timeinfo);

    return timeinfo;  
}

bool PushBullet::closeConnection()
{
    this->secureClient->stop();
    return !this->isConnected();
}